let a: number = 456

// 1 - First function
function isPair (nombre: number): boolean {
    return (nombre % 2 === 0);
}

// 2 - Array parameter
function salut(t: Array<string>): Array<string> {
    let out = [];
    for (let item of t) {
        out.push('Salut' + item);
    }
    return out;
}

// 3 - Callback parameter
function isPair2(nombre: number, callback: (number) => void): boolean {
    return (nombre % 2 === 0);
}

// 4 - Object parameter
function isPair3(nombre: number, options: { a: number, b: string }): boolean {
    return (nombre % 2 === 0);
}

// 5 - Optional parameter
function isPair4(nombre: number, options?: { a: number, b?: string }): boolean {
    return (nombre % 2 === 0);
}

// 6 - 'OR' condition on parameter type
function isPair5(nombre: number | string): boolean {
    if (typeof nombre !== 'number') {
        nombre = parseInt(<string>nombre, 10);
    }
    return (<number>nombre % 2 === 0);
}

// -----------------------------------------------------------------

// 1
console.log(isPair(456))

// 2
salut(['aze', 'aze', 'aze']);

// 3
isPair2(2, function (reste) {
     console.log(reste);
});

// 4
isPair3(2, { a: 2, b: 'aze' });

// 5
isPair4(2, { a: 2 });

// 6
isPair5(2);
isPair5("2");