class Demo2 {

    private _element: string;

    constructor() {
    }

    demo() {

    }

    static demo2 () {

    }

    private demo3 () {

    }

    set element (value: string) {
        this._element = value;
    }

    get element (): string {
        return this._element;
    }
}

class Demo2_2 extends Demo2 {

    public test () {

    }

}

// ---------------------

let d2 = new Demo2();
let d2_2 = new Demo2_2();

// Different methods types
d2.demo();
Demo2.demo2();

// Set variable
d2.element = 'Salut';
console.log(d2.element);

// Additional method for Demo3
d2_2.demo();
d2_2.test();