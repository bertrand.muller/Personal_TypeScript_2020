interface DemoOptions {
    autoplay: boolean,
    x?: number,
    success: (data: string) => void
}

class Demo3 {

    private options;

    constructor(options: DemoOptions) {
        this.options = options;
    }

}

class Options implements DemoOptions {

    autoplay: boolean;

    success(data: string): void {
    }

}

let d3 = new Demo3({
    autoplay: true,
    x: 2,
    success: function (data) {

    }
});