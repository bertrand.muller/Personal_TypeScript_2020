namespace Bertrand {

    let variable_inside_namespace = "test";

    export class Demo4 {

        private options;

        constructor(options) {
            this.options = options;
        }

    }

}

let d4 = new Bertrand.Demo4({
    autoplay: true,
    x: 2,
    success: function (data) {

    }
});