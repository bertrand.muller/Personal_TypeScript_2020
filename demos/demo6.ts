function Component(options) {
    return function (target) {
        console.log(options, target);
    }
}

// Decorate class (can decorate properties, functions, ...)
@Component({
    selector: '.demo6'
})
export default class Demo6 {

    private options;

    constructor(options) {
        this.options = options;
    }

}