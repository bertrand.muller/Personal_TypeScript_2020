import Demo5 from "./demo5";

// Enum (liste de constantes)
enum DIRECTIONS {
    UP,
    DOWN,
    LEFT,
    RIGHT
}

let d5 = new Demo5({
    autoplay: true,
    x: 2,
    success: function (data) {

    },
    direction: DIRECTIONS
});