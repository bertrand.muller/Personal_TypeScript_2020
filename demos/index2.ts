declare let $: JQueryStatic

import Demo7 from './demo7';
import scrollTo from 'scroll-to';

let d7 = new Demo7({
    autoplay: true,
    x: 2
});

scrollTo(500, 1200, {
    ease: 'out-bounce',
    duration: 1500
});

$('.demo').click(function () {
    scrollTo(0, 0);
});