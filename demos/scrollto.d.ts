interface ScrolltoOptions {
    ease?: string,
    duration?: number
}

declare module 'scroll-to' {

    function scrollTo(x: number, y: number, options?: ScrolltoOptions);

    export default scrollTo;

}